/* In the default startup.S for stm32 parts all interrupt handlers
 * are forwarded to the same single default handler.
 * This makes it quite difficult to see which interrupt has fired
 * but missing a handler.
 * Including this file provides a separate default handler for each
 * interrupt, so as soon as the debugger is paused you can see
 * what's fired. Best not to include this in release builds however.
 */
#ifdef __cplusplus
extern "C" {
#endif


#define DEFAULT_HANDLER(handler) \
    void __attribute__((weak,volatile,noreturn)) handler() { \
        printf("Missing Event Handler for "#handler); \
        __asm volatile ("bkpt 0"); \
        while (1) ; \
    }

DEFAULT_HANDLER(NMI_Handler);
DEFAULT_HANDLER(HardFault_Handler);
DEFAULT_HANDLER(SVC_Handler);
DEFAULT_HANDLER(PendSV_Handler);
DEFAULT_HANDLER(SysTick_Handler);
DEFAULT_HANDLER(WWDG_IRQHandler);
DEFAULT_HANDLER(PVD_VDDIO2_IRQHandler);
DEFAULT_HANDLER(RTC_IRQHandler);
DEFAULT_HANDLER(FLASH_IRQHandler);
DEFAULT_HANDLER(RCC_CRS_IRQHandler);
DEFAULT_HANDLER(EXTI0_1_IRQHandler);
DEFAULT_HANDLER(EXTI2_3_IRQHandler);
DEFAULT_HANDLER(EXTI4_15_IRQHandler);
DEFAULT_HANDLER(TSC_IRQHandler);
DEFAULT_HANDLER(DMA1_Ch1_IRQHandler);
DEFAULT_HANDLER(DMA1_Ch2_3_DMA2_Ch1_2_IRQHandler);
DEFAULT_HANDLER(DMA1_Ch4_7_DMA2_Ch3_5_IRQHandler);
DEFAULT_HANDLER(ADC1_COMP_IRQHandler);
DEFAULT_HANDLER(TIM1_BRK_UP_TRG_COM_IRQHandler);
DEFAULT_HANDLER(TIM1_CC_IRQHandler);
DEFAULT_HANDLER(TIM2_IRQHandler);
DEFAULT_HANDLER(TIM3_IRQHandler);
DEFAULT_HANDLER(TIM6_DAC_IRQHandler);
DEFAULT_HANDLER(TIM7_IRQHandler);
DEFAULT_HANDLER(TIM14_IRQHandler);
DEFAULT_HANDLER(TIM15_IRQHandler);
DEFAULT_HANDLER(TIM16_IRQHandler);
DEFAULT_HANDLER(TIM17_IRQHandler);
DEFAULT_HANDLER(I2C1_IRQHandler);
DEFAULT_HANDLER(I2C2_IRQHandler);
DEFAULT_HANDLER(SPI1_IRQHandler);
DEFAULT_HANDLER(SPI2_IRQHandler);
DEFAULT_HANDLER(USART1_IRQHandler);
DEFAULT_HANDLER(USART2_IRQHandler);
DEFAULT_HANDLER(USART3_8_IRQHandler);
DEFAULT_HANDLER(CEC_CAN_IRQHandler);

#ifdef __cplusplus
}
#endif
